import React, {Component, useState} from 'react';
import {Button} from 'react-native-paper';
import {Icon} from 'native-base';

export default function Tile(props) {
  return (
    <Button onPress={() => props.onClickFn(props.id)}>
      <Icon name="trash" />
    </Button>
  );
}
