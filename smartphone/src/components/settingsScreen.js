import React, {useEffect, useState} from 'react';
import { Header,
  Content,
  Left,
  Footer,
  H1,
  Body,
  Right,
  Toast,
  Picker,
  Icon,
  Title,
  Root
} from 'native-base';
import axios from 'axios';
import {StyleSheet, View} from 'react-native';
import {
  ActivityIndicator,
  Button,
  Colors,
  List,
  Paragraph,
  TextInput,
} from 'react-native-paper';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Tile from './tile';
import { ScrollView } from "react-native-gesture-handler";
import CustomHeader from "./header";
export default function SettingsScreen(props) {
  const [ipAddressInput, onChangeIpAddress] = React.useState('IP address');
  const [ipAddressValue, setIpAddressValue] = useState('');
  const [date, setDate] = useState(new Date());
  const [portion, setPortion] = useState('M');
  const [mode, setMode] = useState('time');
  const [show, setShow] = useState(false);
  const [load, setLoad] = useState(false);
  const [jobs, setJobs] = useState([]);
  const [status, setStatus] = useState('Unknown');

  useEffect(() => {
    async function setIp() {
      await setIpState();
      await loadFeedings();
    }
    setIp();
  }, []);

  const IP_ADDRESS_KEY = 'IP_ADDRESS';

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    console.log(show);
    setDate(currentDate);
    console.log(`Nastaven cas na: ${currentDate.toISOString()}`);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showTimepicker = () => {
    showMode('time');
  };

  async function saveIpAddress() {
    try {
      await AsyncStorage.setItem(IP_ADDRESS_KEY, ipAddressInput);
    } catch (e) {
      console.log(e);
      Toast.show({
        text: toastMessages.IP_SAVED_ERROR,
        duration: 2500,
        type: 'danger',
        position: 'center',
      });
    }
    const value = await AsyncStorage.getItem(IP_ADDRESS_KEY);
    console.log(value);
    Toast.show({
      text: toastMessages.IP_SAVED_SUCCESS,
      duration: 2500,
      type: 'success',
      position: 'center',
    });
    setIpAddressValue(value);
    await loadFeedings();
  }

  function printMinutes(minutes) {
    if (minutes < 10) {
      return `0${minutes}`;
    }
    return minutes;
  }

  async function saveFeeding() {
    let data = {
      portion: portion,
      time: date,
    };
    try {
      setLoad(true);
      let ip = await getIpAddress();
      console.log(ip);
      await axios.post(`http://${ip}:${PORT}/feeding/create`, data);
      await loadFeedings();
      setLoad(false);
      console.log(data);
      Toast.show({
        text: toastMessages.FEEDING_SAVED_SUCCESS,
        duration: 2500,
        type: 'success',
        position: 'center',
      });
    } catch (e) {
      Toast.show({
        text: toastMessages.FEEDING_SAVED_SUCCESS,
        duration: 2500,
        type: 'danger',
        position: 'center',
      });
    }
  }

  async function deleteItem(id) {
    console.log(id);
    setLoad(true);
    let ip = await getIpAddress();
    console.log(ip);
    await axios.post(`http://${ip}:${PORT}/feeding/delete`, {id: id});
    await loadFeedings();
    setLoad(false);
  }

  async function loadFeedings() {
    try {
      let ip = await getIpAddress();
      console.log(ip);
      let result = await axios.get(`http://${ip}:${PORT}/feeding/list`);
      let data = result.data;
      console.log(data);
      setJobs(data);
    } catch (e) {
      console.log(e.toLocaleString());
      Toast.show({
        text: toastMessages.GET_JOBS_FAILED,
        duration: 2000,
        type: 'danger',
        position: 'center',
      });
    }
  }

  function renderList(array) {
    return array.map((object) => {
      let date = moment(object.value.time);
      return (
        <List.Item
          key={object.key}
          title={`Feeding on ${date.format('HH')}:${date.format('mm')} 
          Portion: ${object.value.portion}`}
          right={(props) => <Tile onClickFn={deleteItem} id={object.key} />}
        />
      );
    });
  }

  async function setIpState() {
    let ip = await getIpAddress();
    if (ip) {
      setIpAddressValue(ip);
    }
  }

  async function getStatus() {
    let ip = await getIpAddress();
    console.log(ip);
    let result = await axios.get(`http://${ip}:${PORT}/status/get`);
    let data = result.data;
    console.log(data);
    if (data.containerEmpty) {
      setStatus('Empty');
    } else {
      setStatus('Not Empty');
    }
  }

  async function getIpAddress() {
    return await AsyncStorage.getItem(IP_ADDRESS_KEY);
  }

  return (
    <ScrollView>
      <Root>
    <View>
      <Header styles={styles.header}>
        <CustomHeader navigation={props.navigation}/>
        <Left />
        <Body>
          <Title>Smart Fedder</Title>
        </Body>
        <Right />
      </Header>
      <Content>
        <View style={styles.body}>
          <View style={styles.sectionContainer}>
            <H1 style={styles.headline}>{paragraphs.SET_IP_ADDRESS}</H1>
            <TextInput
              label="IP address of the device"
              mode="outlined"
              onChangeText={(text) => onChangeIpAddress(text)}
              value={ipAddressInput}
            />
            <Button
              style={{marginTop: 15}}
              icon="send"
              mode="contained"
              onPress={saveIpAddress}>
              Save IP adress
            </Button>
            {ipAddressValue !== null ? (
              <Paragraph>Saved IP: {ipAddressValue}</Paragraph>
            ) : (
              <Paragraph>No IP saved yet.</Paragraph>
            )}
          </View>
          <View style={styles.sectionContainer}>
            <H1 style={styles.headline}>{paragraphs.SET_TIME}</H1>
            <Button onPress={showTimepicker} icon="send" mode="contained">
              Open time picker
            </Button>
            {date != null && (
              <Paragraph style={styles.feedingText}>
                Time is set at: {date.getHours()} :
                {printMinutes(date.getMinutes())}
              </Paragraph>
            )}
          </View>
          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              value={date}
              mode={mode}
              is24Hour={true}
              display="default"
              onChange={onChange}
            />
          )}
          <View style={styles.sectionContainer}>
            <H1 style={styles.headline}>{paragraphs.SET_PORTION}</H1>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{height: 50}}
              placeholder="Select portion"
              placeholderStyle={{color: '#bfc6ea'}}
              placeholderIconColor="#007aff"
              selectedValue={portion}
              onValueChange={(value) => setPortion(value)}>
              <Picker.Item label="Medium - 20g" value="M" />
              <Picker.Item label="Large - 40g" value="L" />
            </Picker>

            <ActivityIndicator animating={load} color={Colors.blueA100} />
            <Button
              style={{marginTop: 15}}
              icon="send"
              mode="contained"
              onPress={saveFeeding}>
              Save feeding
            </Button>
          </View>
          <View>{renderList(jobs)}</View>
          <View>
            <Button
              style={{marginTop: 15}}
              icon="send"
              mode="contained"
              onPress={getStatus}>
              Get status
            </Button>
            <Paragraph>Status of container: {status}</Paragraph>
          </View>
        </View>
      </Content>
      <Footer>
        <Paragraph style={styles.footer}>
          Vytvořeno v rámci KSMAP/KUMTE 2020/2021
        </Paragraph>
        <Paragraph style={styles.footer}>(c) Viktor Pešek</Paragraph>
      </Footer>
    </View>
      </Root>
    </ScrollView>
  );
}

const toastMessages = {
  IP_SAVED_SUCCESS: 'IP address saved.',
  IP_SAVED_ERROR: 'IP address could not be saved.',
  FEEDING_SAVED_SUCCESS: 'Feeding has been successfully saved.',
  FEEDING_SAVED_FAILEDS: 'Feeding could not be saved. Check your connection.',
  GET_JOBS_FAILED: 'Loading schedules failed',
};

const paragraphs = {
  SET_IP_ADDRESS: 'Set IP address',
  SET_TIME: 'Set time of food schedule',
  SET_PORTION: 'Set portion of food',
  SAVE_IP_TEXT: 'No IP saved.',
};

const PORT = 8080;

const styles = StyleSheet.create({
  header: {
    marginTop: 20
  },
  scrollView: {
    backgroundColor: Colors.black,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.black,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.white,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 12,
    textAlign: 'center',
  },
  headline: {
    marginBottom: 10,
  },
  feedingText: {
    fontSize: 16,
    marginTop: 5,
  },
});
