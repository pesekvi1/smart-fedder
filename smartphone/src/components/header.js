import React, {Component, useState} from 'react';
import {StyleSheet} from 'react-native'
import {Button} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function CustomHeader({ navigation }) {
  return (
    <Button styles={styles.button} onPress={() => navigation.openDrawer()}>
      <Icon name="bars"
            size={35}
            color='#000'
      />
    </Button>
  );
}

const styles = StyleSheet.create({
  button: {
    paddingTop: 8
  }
})
