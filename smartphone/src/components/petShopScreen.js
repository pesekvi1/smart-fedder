import React, {useEffect, useState} from 'react';
import {
  Container,
  Header,
  Content,
  Left,
  Footer,
  H1,
  Body,
  Right,
  Toast,
  Picker,
  Icon,
  Title,
  Root
} from 'native-base';
import { ScrollView } from "react-native-gesture-handler";
import { StyleSheet, View } from "react-native";
import Geolocation from "react-native-geolocation-service";
import CustomHeader from "./header";
var SendIntentAndroid = require("react-native-send-intent");
import { ActivityIndicator, Button, Colors, List, Paragraph, Text, TextInput } from "react-native-paper";
import axios from "axios";
import config from "../config.json";

export default function PetShopScreen(props) {

  const [location, setLocation] = useState(null);
  const [shops, setShops] = useState([]);
  const [load, setLoad] = useState(false);

  useEffect(() =>{
    async function setGpsLocation(){
      await getLocation()
    }
    setGpsLocation()
  }, [])

  async function getLocation() {
    let latitude;
    let longitude;
    Geolocation.getCurrentPosition(position => {
      latitude = position.coords.latitude;
      longitude = position.coords.longitude
      setLocation({ latitude, longitude });
    },(error) => {
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    )
  }

  async function loadNearestShops() {
    setLoad(true)
    if (location !== null) {
      let uri = `https://maps.googleapis.com/maps/api/place/textsearch/json?key=${config.googleApiKey}&location=${location.latitude},${location.longitude}&query=zvire%25krmivo%25chovatelske%25potreby&opennow=true&rankby=distance`;
      console.log(location.latitude)
      console.log(uri);
      let result = await axios.get(uri)
      console.log(result.data);
      setShops(result.data.results)
      setLoad(false)
    }
  }

  function renderList(array) {
    console.log(array);
    return array.map((object) => {
      return (
        <List.Item
          key={object['place_id']}
          title={`${object.name}`}
          description={`${object['formatted_address']}`}
          left={props => <Button icon="navigation" onPress={() => SendIntentAndroid.openMapsWithRoute(`${object.geometry.location.lat},${object.geometry.location.lng}`, 'd')} />}
        />
      );
    });
  }

  return (
    <ScrollView>
      <Root>
        <View>
          <Header>
            <CustomHeader navigation={props.navigation}/>
            <Left />
            <Body>
              <Title>Pet shops</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            <Button onPress={loadNearestShops}>
              Load nearest shops
            </Button>
            <ActivityIndicator animating={load} color={Colors.blueA100} />
            <View>
            {renderList(shops)}
          </View>
          </Content>
          <Footer>
            <Paragraph style={styles.footer}>
              Vytvořeno v rámci KSMAP/KUMTE 2020/2021
            </Paragraph>
            <Paragraph style={styles.footer}>(c) Viktor Pešek</Paragraph>
          </Footer>
        </View>
      </Root>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  footer: {
    color: Colors.white,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 12,
    textAlign: 'center',
  },
});

