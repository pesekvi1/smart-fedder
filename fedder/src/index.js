'use-strict';
let rpio = require('rpio');
let express = require('express');
let fs = require('fs');
const moment = require('moment');
const scheduler = require('node-schedule');

//const Gpio = require('pigpio').Gpio;

const motorSpeed = 1725;
const TIME_ZONE = 'Europe/Prague';

const server = express();

server.use(express.json());

let configFile = require('./config.json');
let scheduleFile = require('./jobs.json');
let scheduleMap = new Map();
/*
https://pinout.xyz/pinout/pin3_gpio2
1           2 power engine
3           4
5           6 GND engine
7           8
9           10
11 sig engine  12
13          14
15          16
17          18
19          20
21          22
23          24
25          26
27          28
29          30
31          32
33          34
35          36
37          38
39          40
*/

let movSensor = 37; //37//GPIO26
let motorPin = 10; //19//GPIO10

//const motor = new Gpio(motorPin, {mode: Gpio.OUTPUT});


function setup() {
  rpio.open(movSensor, rpio.INPUT);
  loadJobs();
}

function loadJobs() {
  if (Object.keys(scheduleFile.jobs).length !== 0) {
    let entries = Object.entries(scheduleFile.jobs);
    for (const [id, job] of entries) {
      const time = moment(job.time).tz(TIME_ZONE);
      let jobInstance = scheduler.scheduleJob(`${time.format('mm')} ${time.format('HH')} * * *`, () => {
        feedDog(job.portion);
      });
      job.jobInstance = jobInstance
      scheduleMap.set(id, job);
    }
    console.log(scheduleMap);
  } else {
    console.log('Schedules are empty.');
  }
}

function motionDetection() {
  let value = rpio.read(movSensor);
  console.log('Motiion detected ' + value);
  return value;
}

function feedProcess() {
  //motor.servoWrite(motorSpeed)
  rpio.msleep(850);
  //motor.servoWrite(1500);
}

function detectMotionOfGrain() {
  let movementDetected = motionDetection();
  if (movementDetected === 0) {
    console.log('Motion not detected, trying again.')
    //motor.servoWrite(motorSpeed)
    rpio.msleep(400);
    //motor.servoWrite(1500);
    let nextDetection = motionDetection();
    if (!nextDetection) {
      configFile.containerEmpty = true;
      fs.writeFileSync('./config.json', JSON.stringify(configFile), err => {
        if (err) {
          throw err;
        }
      });
    }
  } else {
    console.log('Motion detected.')
    configFile.containerEmpty = false;
    fs.writeFileSync('./config.json', JSON.stringify(configFile), err => {
      if (err) {
        throw err;
      }
    });
  }
}

function feedDog(portion) {
  let now = moment();
  console.log(`Feeding process has started on ${now.toJSON()} portion: ${portion}`);
  if (portion === 'M') {
    feedProcess();
  } else if (portion === 'L') {
    feedProcess();
    rpio.msleep(1000);
    feedProcess();
  }
  rpio.msleep(300)
  detectMotionOfGrain();
}

function persistJobs(scheduleMap) {
  scheduleFile.jobs = Object.fromEntries(scheduleMap);
  console.log(Object.fromEntries(scheduleMap));
  fs.writeFileSync('./jobs.json', JSON.stringify(scheduleFile), err => {
    if (err) {
      throw err;
    }
  });
}

server.post('/feeding/create', function (req, res) {
  console.log('Vytvarim novy job.');
  let body = req.body;
  let time = moment(body.time).tz(TIME_ZONE);
  console.log(time.format('HH'));
  console.log(time.format('mm'));
  console.log(body.portion);
  let portion = body.portion;
  let createJob = scheduler.scheduleJob(`${time.format('mm')} ${time.format('HH')} * * *`, () => {
    feedDog(portion);
  });
  let timeStamp = Date.now();
  let newJob = {
    time: time.toJSON(),
    portion: portion,
    jobInstance: createJob,
  };
  scheduleMap.set(timeStamp, newJob);
  persistJobs(scheduleMap);
  res.sendStatus(200);
});

server.post('/feeding/delete', function (req, res) {
  let body = req.body;
  let jobId = body.id;
  console.log(`Job with ID: ${body.id} will be deleted`);
  let oldJob = scheduleMap.get(jobId);
  let instanceOldJob = oldJob.jobInstance;
  instanceOldJob.cancel(false);
  scheduleMap.delete(jobId);
  persistJobs(scheduleMap);
  res.sendStatus(200);
});

server.get('/feeding/list', function (req, res) {
  const result = Array.from(scheduleMap).map(([key, value]) => ({key, value}));
  res.send(result);
});

server.get('/status/get', function (req, res) {
  res.send(configFile);
});

server.get('/name', function (req, res) {
  let json = {name: 'Viktor Pesek'};
  console.log(json);
  res.send(json);
});

server.listen(8080, () => console.log('Server started on port 8080'));
setup();

