# Smart Fedder

Smart Fedder is a device used to feed dogs or cats in defined times each day.

Used technologies
* NodeJS
    * Node schedule
    * Moment
    * Moment-timezone
    * Express
* React Native
    * Native Base
* Raspberry Pi 2
    * HC-SR501 movement sensor
    * MG-996 rotation servo, continuous
    * Breadboard
    
How to run application
* Server
    * Folder **fedder**
    * npm install
    * node src/index.js
* Mobile app
    * Folder **smartphone**
    * npm install
    * npm run android

To run application on your smartphone you have to plug it into your computer or install emulator
More info here: https://reactnative.dev/docs/running-on-device
    
Links with presentation of device and mobile app in React Native
* Mobile app https://www.youtube.com/watch?v=rmv-umjJxlE&ab_channel=ViktorPe%C5%A1ek
* Mobile app (integration with Google API and Google maps) https://www.youtube.com/watch?v=CVZ9dog7ITA      
* Raspberry Pi product https://www.youtube.com/watch?v=wzV0k3BhEJI&ab_channel=ViktorPe%C5%A1ek
